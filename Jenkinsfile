#!/usr/bin/env groovy
def IMAGE_NAME
pipeline{
    agent any
    stages{
        stage("artifact versioning"){
            steps{
                script{
                    echo "artifact versioning"
                    sh "mvn build-helper:parse-version versions:set -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} versions:commit"
                    def extracted = readFile('pom.xml') =~ '<version>(.+)</version>'
                    env.VERSION = extracted[0][1]
                    IMAGE_NAME = "java-maven-app-${VERSION}"
                }
            }
        }

        stage("build artifact"){
            steps{
                echo "build & testing......"
                sh "mvn clean package"
            }
        }

        stage("build image & push"){
            steps{
                script{
                    withCredentials([
                        usernamePassword(
                        credentialsId: 'dockerhub-credentials',
                        usernameVariable: 'USER',
                        passwordVariable: 'PASS')
                    ]){
                        sh "docker build -t my-app:${IMAGE_NAME} ."
                        sh "echo ${PASS} | docker login -u ${USER} --password-stdin"
                        sh "docker tag my-app:${IMAGE_NAME} golebu2023/image-registry:${IMAGE_NAME}"
                        sh "docker push golebu2023/image-registry:${IMAGE_NAME}"
                    }
                }
            }
        }

        stage("deploy"){
            steps{
                script{
                    echo "deploying...."
                    def dockerCmd =  "sh ./dockerRun.sh 'golebu2023/image-registry:${IMAGE_NAME}'"
                    sshagent(['deploy-key']) {
                        sh "scp docker-compose.yaml root@134.122.38.248:/root"
                        sh "scp dockerRun.sh root@134.122.38.248:/root"
                        sh "ssh -o StrictHostKeyChecking=no root@134.122.38.248 chmod +x /root/dockerRun.sh"
                        sh "ssh -o StrictHostKeyChecking=no root@134.122.38.248 ${dockerCmd}"
                    }
                }
            }
        }

        stage("bump-up version"){
            steps{
                script{
                 echo "bump-up versioning"
                    withCredentials([
                        usernamePassword(
                        credentialsId: "gitlab-credentials",
                        usernameVariable: 'USER',
                        passwordVariable: 'PASS')
                    ]){
                        sh "git config --global user.name 'jenkins'"
                        sh "git config --global user.email 'jenkins@gmail.com'"
                        sh "git add ."
                        sh "git commit -m 'ci-bump'"
                        sh "git remote set-url origin https://${USER}:${PASS}@gitlab.com/cgolebu/complete-automted-cicd-pipeline.git"
                        sh "git push origin HEAD:main"
                    }
                }

            }
        }
    }
}